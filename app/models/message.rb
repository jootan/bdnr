class Message
  include Mongoid::Document
  field :title, type: String
  field :body, type: String
  field :multimedia, type: String
  field :links, type: String
end
